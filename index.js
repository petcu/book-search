/* Get json index */
async function getSourceIndex() {
  const response = await fetch(
    "https://book-search-livid.vercel.app/index/all.json.gz"
  );
  const responseBuffer = new Uint8Array(await response.arrayBuffer());
  const decompressedStream = fflate.decompressSync(responseBuffer);
  const responseJSON = JSON.parse(new TextDecoder().decode(decompressedStream));
  return responseJSON;
}

/* Alpine Logic */
document.addEventListener("alpine:init", async () => {
  Alpine.data("main", () => ({
    /* Variables */
    filterOpen: false,
    filterSize: 10,
    filterFormat: "",
    results: [],
    resultsNotFound: false,
    input: "",
    fuse: undefined,

    /* Initialize */
    async init() {
      const books = await getSourceIndex();
      this.fuse = new Fuse(books, { keys: ["title"] });
    },

    /* Functions */
    getResults() {
      this.results = this.fuse.search(this.input);
      console.log("before: " + this.results.length);
      // Run Filters
      this.filterByFormat();
      this.filterBySize();
      // Display feedback if nothing was found
      console.log("after: " + this.results.length);
      this.resultsNotFound = this.results.length == 0;
    },
    filterBySize() {
      // Slice the results
      this.results = this.results.slice(0, this.filterSize);
    },
    filterByFormat() {
      // No filter, skip step
      if (this.filterFormat === "")
        return;
      // Filter detected
      this.results = this.results.filter(
        (res) => res.item.format === this.filterFormat
      );
    },

  }));
});
