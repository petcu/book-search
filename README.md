# Book-Search

## Branch: Static version

## Notes
Ok, so... Someday, Mongo Atlas stopped working, and I didn't want to fix it.
Also, updating the sveltekit will be a pain, either way.

In such a situation, making everything self-sustainable should be my cheat code for the time being.
I updated (rewrote) everything to be client-oriented, no server, no problems - hopefully.

I have no plans for what I want to do with it in the future.